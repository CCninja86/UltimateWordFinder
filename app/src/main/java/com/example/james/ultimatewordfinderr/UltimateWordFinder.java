package com.example.james.ultimatewordfinderr;

import android.app.Application;
import android.util.Log;

import io.objectbox.BoxStore;
import io.objectbox.android.AndroidObjectBrowser;

public class UltimateWordFinder extends Application {

    public static final String TAG = "ObjectBox";
    public static final boolean EXTERNAL_DIR = false;

    private BoxStore boxStore;

    @Override
    public void onCreate(){
        super.onCreate();

        boxStore = MyObjectBox.builder().androidContext(UltimateWordFinder.this).build();

        if(BuildConfig.DEBUG){
            new AndroidObjectBrowser(boxStore).start(this);
        }

        Log.d("UltimateWordFinder", "Using ObjectBox " + BoxStore.getVersion() + " (" + BoxStore.getVersionNative() + ")");
    }

    public BoxStore getBoxStore(){
        return boxStore;
    }
}
