package com.example.james.ultimatewordfinderr;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link WordFinderMainFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link WordFinderMainFragment#newInstance} factory method toJa
 * create an instance of this fragment.
 */
public class WordFinderMainFragment extends Fragment implements PatternMatcherResultsListener {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private com.example.james.ultimatewordfinderr.Dictionary dictionary;
    private ArrayList<Word> matches;
    private ArrayList<String> selectedWords;
    private ArrayList<String> advancedSearchTextFilters;
    private int numLetterFilter;

    private EditText editTextLettersBoard;
    private EditText editTextLettersRack;
    private CheckBox checkOnlyLettersRack;
    private CheckBox checkIncludeKnown;
    private TextView textViewWordProgress;
    private ProgressDialog progressDialog;

    private Stopwatch stopwatch;


    private boolean textFlag;

    private PatternMatcher patternMatcher;

    private View callingView;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment WordFinderMainFragment.
     */

    public static WordFinderMainFragment newInstance(String param1, String param2) {
        WordFinderMainFragment fragment = new WordFinderMainFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public WordFinderMainFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    public void backButtonWasPressed() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_word_finder_main, container, false);
        Globals g = Globals.getInstance();
        this.dictionary = g.getDictionary();

        this.patternMatcher = new PatternMatcher(this, getActivity().getApplication());

        this.matches = new ArrayList<Word>();
        this.selectedWords = new ArrayList<String>();
        this.advancedSearchTextFilters = new ArrayList<String>();
        this.numLetterFilter = 0;

        editTextLettersBoard = (EditText) view.findViewById(R.id.editTextLettersBoard);
        editTextLettersRack = (EditText) view.findViewById(R.id.editTextLettersRack);
        Button btnSearch = (Button) view.findViewById(R.id.btnSearch);
        Button btnAdvancedSearch = (Button) view.findViewById(R.id.btnAdvancedSearch);
        Button btnExample = (Button) view.findViewById(R.id.btnExample);

        editTextLettersRack.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnExample.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Word Finder Example");
                builder.setMessage("Letters on Board: f????t\n" +
                        "Letters in Rack: orebstu\n\n" +
                        "Results:\n\n" +
                        "forest\n" +
                        "forset\n" +
                        "fouett\n" +
                        "froust\n" +
                        "fustet");

                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
            }

        });

        btnSearch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                callingView = view;

                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Searching Dictionary...");
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(false);
                progressDialog.show();

                String boardRegex = patternMatcher.generateBoardRegex(editTextLettersBoard.getText().toString());
                patternMatcher.getAllWordsMatchingRegex(boardRegex);
            }
        });

        btnAdvancedSearch.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                mListener.onSearchFragmentInteraction(view, null);
            }
        });

        return view;
    }

    @Override
    public void onPatternMatcherGetAllWordsMatchingRegexTaskComplete(ArrayList<Word> matches) {
        if (editTextLettersRack.getText().toString().isEmpty()) {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null;
            }

            if (matches.size() > 0) {
                mListener.onSearchFragmentInteraction(callingView, matches);
            } else {
                Toast.makeText(getActivity(), "No results found", Toast.LENGTH_SHORT).show();
            }
        } else {
            patternMatcher.matchWithPlayerPattern(matches, editTextLettersRack.getText().toString(), editTextLettersBoard.getText().toString());
        }

    }

    @Override
    public void onPatternMatcherMatchWithPlayerPatternTaskComplete(ArrayList<Word> matches) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }

        if (matches.size() > 0) {
            mListener.onSearchFragmentInteraction(callingView, matches);
        } else {
            Toast.makeText(getActivity(), "No results found", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        public void onSearchFragmentInteraction(View view, ArrayList<Word> searchMatches);
    }
}