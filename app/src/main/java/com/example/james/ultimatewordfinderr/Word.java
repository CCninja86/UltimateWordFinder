package com.example.james.ultimatewordfinderr;

import java.io.Serializable;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

/**
 * Created by James on 19/11/2015.
 */
@Entity
public class Word implements Serializable {

    @Id
    public long id;

    private String word;
    private boolean word_is_official;

    public Word() {

    }

    public Word(String word, boolean word_is_official) {
        this.setWord(word);
        this.setWord_is_official(word_is_official);
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public boolean isWord_is_official() {
        return word_is_official;
    }

    public void setWord_is_official(boolean word_is_official) {
        this.word_is_official = word_is_official;
    }
}
