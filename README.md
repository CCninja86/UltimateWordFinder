This project is licensed under the terms of the GPL-2.0 license.

### Download

1. Before you download the app, you must opt-in to the Beta here: https://play.google.com/apps/testing/nz.ac.aut.ultimatewordfinderr
2. After you have opted-in to the Beta, you can download the app on the Google Play store here: https://play.google.com/store/apps/details?id=nz.ac.aut.ultimatewordfinderr
